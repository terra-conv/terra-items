use super::Item;
use crate::Prefix;
use terra_types::PositiveI32;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ItemSlot {
	pub item: Item,
	pub prefix: Option<Prefix>,
	pub count: PositiveI32,
}

impl From<SingleItemSlot> for ItemSlot {
	fn from(value: SingleItemSlot) -> Self {
		ItemSlot {
			item: value.item,
			prefix: value.prefix,
			count: PositiveI32::new(1).expect("In bounds"),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SingleItemSlot {
	pub item: Item,
	pub prefix: Option<Prefix>,
}

impl From<ItemSlot> for SingleItemSlot {
	fn from(item_slot: ItemSlot) -> Self {
		SingleItemSlot {
			item: item_slot.item,
			prefix: item_slot.prefix,
		}
	}
}
